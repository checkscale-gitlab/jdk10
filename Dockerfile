ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jre10/master:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjdk-10-jdk
